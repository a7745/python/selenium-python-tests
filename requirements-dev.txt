black==22.1.0
invoke==1.7.0
isort==5.10.1
flake8==4.0.1
-r requirements.txt
