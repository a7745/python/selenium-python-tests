from invoke import task


@task
def lint(c):
    target = "tasks.py pages/ test_cases"
    c.run("black {}".format(target))
    c.run("isort {}".format(target))
    c.run("flake8 {}".format(target))
