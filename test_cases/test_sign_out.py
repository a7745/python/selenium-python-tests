from pages import HomePage, SignInPage
from test_cases.base import TestBase


class TestSignOut(TestBase):
    def setup_method(self):
        self.home_page = HomePage(self.driver)
        self.sign_in_page = SignInPage(self.driver)

        self.home_page.open_home_page(self.properties["BASE_URL"])
        self.home_page.go_to_sign_in()
        self.sign_in_page.sign_in(self.secrets["USERNAME"], self.secrets["PASSWORD"])

    def test_sign_out(self):
        self.home_page.sign_out()
