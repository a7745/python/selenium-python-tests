import json
import subprocess

import pytest
from selenium import webdriver


@pytest.fixture(autouse=True)
def init_driver(request, selenium):
    driver = selenium
    request.cls.driver = driver
    yield
    driver.close()


@pytest.fixture(autouse=True)
def resolve_environment(request):
    output = subprocess.check_output(["sops", "-d", "environment/secrets-dev.json"])
    secrets = json.loads(output.decode("utf-8"))
    request.cls.secrets = secrets

    with open("environment/properties-dev.json") as file:
        properties = json.load(file)
        request.cls.properties = properties

    yield
