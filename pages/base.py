from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait


class PageBase:
    def __init__(self, driver: webdriver.Remote) -> None:
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 15)
