from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from pages.base import PageBase


class HomePage(PageBase):
    WELCOME_TEXT = (By.XPATH, "//h1[.='Welcome to Address Book']")
    LINK_SIGN_IN = (By.ID, "sign-in")
    LINK_SIGN_OUT = (By.XPATH, "//a[.='Sign out']")

    def open_home_page(self, url):
        self.driver.get(url)
        self.wait.until(EC.visibility_of_element_located(self.WELCOME_TEXT))

    def go_to_sign_in(self):
        self.driver.find_element(*self.LINK_SIGN_IN).click()
        self.wait.until(EC.title_contains("Sign In"))

    def sign_out(self):
        self.driver.find_element(*self.LINK_SIGN_OUT).click()
        self.wait.until(EC.title_contains("Sign In"))
