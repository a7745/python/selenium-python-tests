from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from pages.base import PageBase


class SignInPage(PageBase):
    INPUT_EMAIL = (By.ID, "session_email")
    INPUT_PASSWORD = (By.ID, "session_password")
    BTN_SIGN_IN = (By.NAME, "commit")

    TEXT_CURRENT_USER = (By.XPATH, "//span[@data-test='current-user']")

    def sign_in(self, email, password):
        self.driver.find_element(*self.INPUT_EMAIL).send_keys(email)
        self.driver.find_element(*self.INPUT_PASSWORD).send_keys(password)
        self.driver.find_element(*self.BTN_SIGN_IN).click()

    def verify_logged_in(self, user_email):
        self.wait.until(
            EC.text_to_be_present_in_element(self.TEXT_CURRENT_USER, user_email)
        )
